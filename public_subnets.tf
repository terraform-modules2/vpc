resource "aws_subnet" "public_subnet_a" {
  vpc_id     = aws_vpc.primary_vpc.id
  cidr_block = var.subnet__public__cidr_a
  availability_zone = var.subnet__public__az_a
  map_public_ip_on_launch = true # Since this is a public subnet - this should always be true

  tags = {
    Name = var.subnet__public__tags_name_a
    group = var.networking_group_tag
  }

  depends_on = [
    aws_vpc.primary_vpc
  ]
}

resource "aws_subnet" "public_subnet_b" {
  vpc_id     = aws_vpc.primary_vpc.id
  cidr_block = var.subnet__public__cidr_b
  availability_zone = var.subnet__public__az_b
  map_public_ip_on_launch = true # Since this is a public subnet - this should always be true

  tags = {
    Name = var.subnet__public__tags_name_b
    group = var.networking_group_tag
  }

  depends_on = [
    aws_vpc.primary_vpc
  ]
}

resource "aws_subnet" "public_subnet_c" {
  vpc_id     = aws_vpc.primary_vpc.id
  cidr_block = var.subnet__public__cidr_c
  availability_zone = var.subnet__public__az_c
  map_public_ip_on_launch = true # Since this is a public subnet - this should always be true

  tags = {
    Name = var.subnet__public__tags_name_c
    group = var.networking_group_tag
  }

  depends_on = [
    aws_vpc.primary_vpc
  ]
}