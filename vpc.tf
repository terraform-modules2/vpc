variable "vpc__cidr_block" {type = string}
variable "vpc__instance_tenancy" {type = string}
variable "vpc__tags_name" {type = string}
variable "vpc__enable_dns_hostnames" {type = bool}
variable "vpc__enable_dns_support" {type = bool}
variable "vpc__internet_gateway__tags_name" {type = string}
variable "vpc__route_table_private_tags_name" {type = string}
variable "vpc__route_table_public_tags_name" {type = string}

resource "aws_vpc" "primary_vpc" {
  cidr_block = var.vpc__cidr_block
  instance_tenancy = var.vpc__instance_tenancy

  tags = {
    Name = var.vpc__tags_name
    group = var.networking_group_tag
  }

  # These 2 settings should be true when we move back to using VPC endpoints. Currently
  # we are unable to do so as SES does not have an endpoint - so for the time being we
  # are relying on a NAT gateway to handle traffic between the other AWS services.
  enable_dns_hostnames = var.vpc__enable_dns_hostnames
  enable_dns_support = var.vpc__enable_dns_support
}

resource "aws_internet_gateway" "vpc_internet_gateway" {
  vpc_id = aws_vpc.primary_vpc.id
  tags = {
    Name = var.vpc__internet_gateway__tags_name
  }

  depends_on = [aws_vpc.primary_vpc]
}

resource "aws_nat_gateway" "vpc_nat_gateway" {
  allocation_id = aws_eip.vpc_nat_gateway_eip.allocation_id
  subnet_id = aws_subnet.public_subnet_a.id

  depends_on = [
    aws_vpc.primary_vpc,
    aws_eip.vpc_nat_gateway_eip,
  ]
}

resource "aws_eip" "vpc_nat_gateway_eip" {
  vpc = true
}

resource "aws_route_table" "private_route_table" {
  vpc_id = aws_vpc.primary_vpc.id

  route {
    cidr_block = "0.0.0.0/0"
    nat_gateway_id = aws_nat_gateway.vpc_nat_gateway.id
  }

  tags = {
    Name = var.vpc__route_table_private_tags_name
  }

  depends_on = [
    aws_vpc.primary_vpc,
    aws_nat_gateway.vpc_nat_gateway
  ]
}

resource "aws_route_table" "public_route_table" {
  vpc_id = aws_vpc.primary_vpc.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.vpc_internet_gateway.id
  }

  tags = {
    Name = var.vpc__route_table_public_tags_name
  }

  depends_on = [
    aws_vpc.primary_vpc,
    aws_internet_gateway.vpc_internet_gateway
  ]
}

# Associate the subnets with our main shiftie route table
# Public Subnets
resource "aws_route_table_association" "route_table_public_subnet_a" {
  route_table_id = aws_route_table.public_route_table.id
  subnet_id = aws_subnet.public_subnet_a.id

  depends_on = [
    aws_route_table.public_route_table,
    aws_subnet.public_subnet_a
  ]
}

resource "aws_route_table_association" "route_table_public_subnet_b" {
  route_table_id = aws_route_table.public_route_table.id
  subnet_id = aws_subnet.public_subnet_b.id

  depends_on = [
    aws_route_table.public_route_table,
    aws_subnet.public_subnet_b
  ]
}

resource "aws_route_table_association" "route_table_public_subnet_c" {
  route_table_id = aws_route_table.public_route_table.id
  subnet_id = aws_subnet.public_subnet_c.id

  depends_on = [
    aws_route_table.public_route_table,
    aws_subnet.public_subnet_c
  ]
}

# Private Subnets
resource "aws_route_table_association" "route_table_private_subnet_a" {
  route_table_id = aws_route_table.private_route_table.id
  subnet_id = aws_subnet.private_subnet_a.id

  depends_on = [
    aws_route_table.private_route_table,
    aws_subnet.private_subnet_a
  ]
}

resource "aws_route_table_association" "route_table_private_subnet_b" {
  route_table_id = aws_route_table.private_route_table.id
  subnet_id = aws_subnet.private_subnet_b.id

  depends_on = [
    aws_route_table.private_route_table,
    aws_subnet.private_subnet_b
  ]
}

resource "aws_route_table_association" "route_table_private_subnet_c" {
  route_table_id = aws_route_table.private_route_table.id
  subnet_id = aws_subnet.private_subnet_c.id

  depends_on = [
    aws_route_table.private_route_table,
    aws_subnet.private_subnet_c
  ]
}