# vpc

A generic VPC with: 
 - 3 public subnets
 - 3 private subnets
 - 1 NAT Gateway
 - 1 Internet Gateway
 - 1 EIP