resource "aws_subnet" "private_subnet_a" {
  vpc_id     = aws_vpc.primary_vpc.id
  cidr_block = var.subnet__private__cidr_a
  availability_zone = var.subnet__private__az_a

  tags = {
    Name = var.subnet__private__tags_name_a
    group = var.networking_group_tag
  }

  depends_on = [
    aws_vpc.primary_vpc
  ]
}

resource "aws_subnet" "private_subnet_b" {
  vpc_id     = aws_vpc.primary_vpc.id
  cidr_block = var.subnet__private__cidr_b
  availability_zone = var.subnet__private__az_b

  tags = {
    Name = var.subnet__private__tags_name_b
    group = var.networking_group_tag
  }

  depends_on = [
    aws_vpc.primary_vpc
  ]
}

resource "aws_subnet" "private_subnet_c" {
  vpc_id     = aws_vpc.primary_vpc.id
  cidr_block = var.subnet__private__cidr_c
  availability_zone = var.subnet__private__az_c

  tags = {
    Name = var.subnet__private__tags_name_c
    group = var.networking_group_tag
  }

  depends_on = [
    aws_vpc.primary_vpc
  ]
}