variable "subnet__public__cidr_a" {type = string}
variable "subnet__public__cidr_b" {type = string}
variable "subnet__public__cidr_c" {type = string}
variable "subnet__public__az_a" {type = string}
variable "subnet__public__az_b" {type = string}
variable "subnet__public__az_c" {type = string}
variable "subnet__public__tags_name_a" {type = string}
variable "subnet__public__tags_name_b" {type = string}
variable "subnet__public__tags_name_c" {type = string}

variable "subnet__private__cidr_a" {type = string}
variable "subnet__private__cidr_b" {type = string}
variable "subnet__private__cidr_c" {type = string}
variable "subnet__private__az_a" {type = string}
variable "subnet__private__az_b" {type = string}
variable "subnet__private__az_c" {type = string}
variable "subnet__private__tags_name_a" {type = string}
variable "subnet__private__tags_name_b" {type = string}
variable "subnet__private__tags_name_c" {type = string}

variable "networking_group_tag" {type = string}